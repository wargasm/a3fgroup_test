# Решение задания 1

Простой агригирующий запрос с постфильтрацией

    select p.name
    from person p 
    left join friends f 
    on p.id = f.person 
    group by id
    having count(p.id) > 5;

    name|
    ----+
    Ted |
    Fibi|

Более сложный запрос, для определения "взаимной дружбы" исползуется присоединение таблицы к самой себе, далее создается синтетическая колонка в которой хранятся данные не зависящие от последовательности колонок друзей, по этой колонке происходит группировка для исключения дублей. 

    select 
    case
        when f1.person > f1.whith_whom 
            then concat(p2."name", ' ', p1."name" )
        else 
            concat(p1."name", ' ', p2."name" )
    end as together
    from friends f1
        left join friends f2
            on f1.person = f2.whith_whom and f2.person = f1.whith_whom 
        left join person p1 
            on p1.id = f1.person 
        left join person p2
            on p2.id = f1.whith_whom  
        where f2.person is not null
        group by together;

    together |
    ---------+
    Ted Jane |
    Jane Mike|
    Ted Mike |

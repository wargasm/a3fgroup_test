
CREATE TABLE public.friends (
    person integer,
    whith_whom integer
);

CREATE SEQUENCE public.person_series_idx
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;

CREATE TABLE public.person (
    id integer DEFAULT nextval('public.person_series_idx'::regclass) NOT NULL,
    name character varying(255)
);

INSERT INTO public.friends VALUES (1, 2);
INSERT INTO public.friends VALUES (1, 3);
INSERT INTO public.friends VALUES (2, 3);
INSERT INTO public.friends VALUES (3, 1);
INSERT INTO public.friends VALUES (3, 4);
INSERT INTO public.friends VALUES (1, 5);
INSERT INTO public.friends VALUES (2, 7);
INSERT INTO public.friends VALUES (8, 1);
INSERT INTO public.friends VALUES (8, 2);
INSERT INTO public.friends VALUES (8, 3);
INSERT INTO public.friends VALUES (8, 4);
INSERT INTO public.friends VALUES (8, 5);
INSERT INTO public.friends VALUES (8, 6);
INSERT INTO public.friends VALUES (8, 7);
INSERT INTO public.friends VALUES (8, 9);
INSERT INTO public.friends VALUES (4, 1);
INSERT INTO public.friends VALUES (1, 4);
INSERT INTO public.friends VALUES (1, 6);
INSERT INTO public.friends VALUES (1, 9);
INSERT INTO public.friends VALUES (4, 3);

INSERT INTO public.person VALUES (1, 'Ted');
INSERT INTO public.person VALUES (2, 'Joe');
INSERT INTO public.person VALUES (3, 'Jane');
INSERT INTO public.person VALUES (4, 'Mike');
INSERT INTO public.person VALUES (5, 'Ross');
INSERT INTO public.person VALUES (6, 'Sam');
INSERT INTO public.person VALUES (7, 'Rachel');
INSERT INTO public.person VALUES (8, 'Fibi');
INSERT INTO public.person VALUES (9, 'Nona');


SELECT pg_catalog.setval('public.person_series_idx', 1, false);

ALTER TABLE ONLY public.person
    ADD CONSTRAINT person_pkey PRIMARY KEY (id);



